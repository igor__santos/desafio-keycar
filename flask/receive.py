

import time


import json
import requests

from google.cloud import pubsub_v1
def save_msg(message):
    msg = message.split('/')
    print(msg[0])
    print(msg[1])

    return requests.post('http://localhost:5000/save',
                        json={
                            'date':msg[0],
                            'message':msg[1],
                            'user_nick':msg[2]
                        })


project_id = 'desafio-123'
subscription_name = 'sub'
subscriber = pubsub_v1.SubscriberClient()
subscription_path = subscriber.subscription_path(
    project_id, subscription_name)
def callback(message):
    print(message)
    msg = bytes.decode(message.data)
    msg_conv = msg.split('Mensagem:')
    print(msg_conv[1])
    save_msg(msg_conv[1])
    message.ack()

subscriber.subscribe(subscription_path, callback=callback)
print('Listening for messages on {}'.format(subscription_path))
while True:
    time.sleep(60)


    
   