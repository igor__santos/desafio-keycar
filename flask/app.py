# flask_web/app.py

from flask import Flask,jsonify
from flask import request
from datetime import datetime
  
app = Flask(__name__)
import psycopg2
from psycopg2.extras import RealDictCursor
import json


app = Flask(__name__)

@app.route('/save',methods=['POST'])
def add_msg():

    if request.form:
        content = [item for item in request.form]
        print (content)
    else:
        content = request.get_json(force=True)

    message = content.get('message')
    date = content.get('date')
    user_nick = content.get('user_nick')

    con = psycopg2.connect(host='localhost', database='desafio',
    user='postgres',port=5433)
    cur = con.cursor()
    sql = "insert into messages(user_nick,message,date)values('%s','%s','%s')"%(user_nick,message,date)  
    cur.execute(sql)
    con.commit()
    con.close()
    return 'ok', 201

    
    

@app.route('/get',methods=['GET'])
def hello_world():

    con = psycopg2.connect(host='localhost', database='desafio',
    user='postgres',port=5433)
    cur = con.cursor()
    sql = "select id,user_nick as user,message as menssagem,TO_CHAR(current_date, 'MM-DD-YYYY') as Data,TO_CHAR(date, 'HH24:MI:SS') as Hora  from messages"  
    cur.execute(sql)
    con.commit()
    result = cur.fetchall()
    con.close()
        
    rs = json.dumps(result)
    
    return rs ,200


@app.route('/get/<string:lang>',methods=['GET'])
def get_msg_id(lang):
    print(lang)
    con = psycopg2.connect(host='localhost', database='desafio',
    user='postgres',port=5433)
    cur = con.cursor()
    sql = "select id,user_nick as user,message as menssagem,TO_CHAR(current_date, 'MM-DD-YYYY') as Data,TO_CHAR(date, 'HH24:MI:SS') as Hora  from messages where user_nick ='%s'"%lang   
    cur.execute(sql)
    con.commit()
    result = cur.fetchall()
    con.close()
    rs = str(result)
    return rs ,200
   
    

@app.route('/send',methods=['POST'])
def send_msg():
    if request.form:
        content = [item for item in request.form]
        print (content)
    else:
        content = request.get_json(force=True)
        message = content.get('message')
        user_nick = content.get('user_nick')

        
    now = datetime.now()
    date = str(now.year)+"-"+str(now.month)+"-"+str(now.day)+" "+str(now.hour)+":"+str(now.minute)+":"+str(now.second) 
    # concatena os dados do nick do usuario junto com a hora de envio da msg e a mensagem
    data = date+"/"+message+"/"+user_nick 
    print(date)
    #chama o metodo que adiciona a mensagem com suas informações a fila
    publish_messages('desafio-123','topico',data)
    return 'ok',200



def publish_messages(project_id, topic_name, message):
    """Publishes multiple messages to a Pub/Sub topic."""
    from google.cloud import pubsub_v1
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)
    data = u'Mensagem: {}'.format(message)
    data = data.encode('utf-8')
    future = publisher.publish(topic_path, data=data)



    




if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')